import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ShortLink {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  longUrl: string;

  @Column()
  expirationTime: Date;

  @Column()
  shortUrl: string;
}
