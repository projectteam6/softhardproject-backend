import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { Material } from './entities/material.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MaterialsService {
  constructor(
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}
  create(createMaterialDto: CreateMaterialDto) {
    return this.materialsRepository.save(createMaterialDto);
  }

  findAll() {
    return this.materialsRepository.find();
  }

  findOne(id: number) {
    return this.materialsRepository.findOne({ where: { id } });
  }

  findType(unit: string) {
    // console.log(type);
    return this.materialsRepository.find({ where: { unit: unit } });
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    const material = await this.materialsRepository.findOneBy({ id });
    if (!material) {
      throw new NotFoundException();
    }
    const updatedMaterial = { ...material, ...updateMaterialDto };

    return this.materialsRepository.save(updatedMaterial);
  }

  async remove(id: number) {
    const material = await this.materialsRepository.findOneBy({ id });
    if (!material) {
      throw new NotFoundException();
    }
    return this.materialsRepository.remove(material);
  }
}
