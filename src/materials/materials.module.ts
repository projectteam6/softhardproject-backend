import { Module } from '@nestjs/common';
import { MaterialsService } from './materials.service';
import { MaterialsController } from './materials.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Material } from './entities/material.entity';
import { OrderItem } from 'src/orders/entities/order-item';
import { ChecklistItem } from 'src/checklist/entities/checklist_item.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Material, OrderItem, ChecklistItem])],
  controllers: [MaterialsController],
  providers: [MaterialsService],
})
export class MaterialsModule {}
