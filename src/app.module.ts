import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Food } from './foods/entities/food.entity';
import { FoodsModule } from './foods/foods.module';
import { Table } from './tables/entities/table.entity';
import { TablesModule } from './tables/tables.module';
import { User } from './users/entities/user.entity';
import { UsersModule } from './users/users.module';
import { OrdersModule } from './orders/orders.module';
import { Order } from './orders/entities/order.entity';
import { EmployeesModule } from './employees/employees.module';
import { Employee } from './employees/entities/employee.entity';
import { OrderItem } from './orders/entities/order-item';
import { SalarysModule } from './salarys/salarys.module';
import { Salary } from './salarys/entities/salary.entity';
import { CheckinscheckoitsModule } from './checkinscheckoits/checkinscheckoits.module';
import { Checkinscheckoit } from './checkinscheckoits/entities/checkinscheckoit.entity';
import { AuthModule } from './auth/auth.module';
import { FoodQueueModule } from './food-queue/food-queue.module';
import { FoodQueue } from './food-queue/entities/food-queue.entity';
import { ShortLinkModule } from './short-link/short-link.module';
import { ShortLink } from './short-link/entities/short-link.entity';
import { ServeModule } from './serve/serve.module';
import { MaterialsModule } from './materials/materials.module';
import { Material } from './materials/entities/material.entity';
import { ChecklistModule } from './checklist/checklist.module';
import { Checklist } from './checklist/entities/checklist.entity';
import { ChecklistItem } from './checklist/entities/checklist_item.entity';

@Module({
  imports: [
    FoodsModule,
    TablesModule,
    UsersModule,
    EmployeesModule,
    FoodQueueModule,
    SalarysModule,
    ShortLinkModule,
    MaterialsModule,
    Material,
    CheckinscheckoitsModule,
    TypeOrmModule.forRoot({
      // type: 'mysql',
      // host: 'localhost',
      // port: 3306,
      // username: 'soft-hard',
      // password: 'Pass1234',
      // database: 'soft-hard',

      type: 'mysql',
      host: 'db-restaurant.ckb2gihhf5t1.ap-southeast-1.rds.amazonaws.com',
      port: 3306,
      username: 'admin',
      password: 'Pass1234',
      database: 'SoftHard',

      // type: 'mysql',
      // host: 'db4free.net',
      // port: 3306,
      // username: 'soft_hard_4free',
      // password: 'Pass1234',
      // database: 'soft_hard_4free',
      entities: [
        Food,
        User,
        Table,
        Order,
        OrderItem,
        Employee,
        Salary,
        ShortLink,
        FoodQueue,
        Checkinscheckoit,
        MaterialsModule,
        Material,
        Checklist,
        ChecklistItem,
      ],
      synchronize: true,
    }),
    OrdersModule,
    AuthModule,
    SalarysModule,
    ShortLinkModule,
    ServeModule,
    MaterialsModule,
    Material,
    ChecklistModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
