import { Employee } from 'src/employees/entities/employee.entity';
// import * as moment from 'moment';
import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Checkinscheckoit {
  @PrimaryGeneratedColumn()
  id: number;
  @CreateDateColumn()
  timestart: Date;
  @Column()
  timeend: Date = new Date();
  @Column()
  hour: number;
  @Column()
  status: string;

  @ManyToOne(() => Employee, (employee) => employee.checkincheckout)
  employee: Employee;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deleteddAt: Date;
}
