import { Test, TestingModule } from '@nestjs/testing';
import { CheckinscheckoitsController } from './checkinscheckoits.controller';
import { CheckinscheckoitsService } from './checkinscheckoits.service';

describe('CheckinscheckoitsController', () => {
  let controller: CheckinscheckoitsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckinscheckoitsController],
      providers: [CheckinscheckoitsService],
    }).compile();

    controller = module.get<CheckinscheckoitsController>(
      CheckinscheckoitsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
