import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCheckinscheckoitDto } from './dto/create-checkinscheckoit.dto';
import { UpdateCheckinscheckoitDto } from './dto/update-checkinscheckoit.dto';
import { Checkinscheckoit } from './entities/checkinscheckoit.entity';

@Injectable()
export class CheckinscheckoitsService {
  constructor(
    @InjectRepository(Checkinscheckoit)
    private ciosRepository: Repository<Checkinscheckoit>,
  ) {}

  create(createCheckinscheckoitDto: CreateCheckinscheckoitDto) {
    return this.ciosRepository.save(createCheckinscheckoitDto);
  }

  findAll() {
    return (
      this.ciosRepository
        .createQueryBuilder('checkinscheckoits')
        .leftJoinAndSelect('checkinscheckoits.employee', 'employee')
        // .leftJoinAndSelect('order.employee', 'employee')
        // .leftJoinAndSelect('order.table', 'table')
        .where('checkinscheckoits.hour = :hour', { hour: 0 })
        .getMany()
    );
  }

  findOne(id: number) {
    return this.ciosRepository.findOne({ where: { id } });
  }

  async update(
    id: number,
    updateCheckinscheckoitDto: UpdateCheckinscheckoitDto,
  ) {
    const checkincheckout = await this.ciosRepository.findOneBy({ id: id });
    if (!checkincheckout) {
      throw new NotFoundException();
    }
    const updateEmployee = { ...checkincheckout, ...updateCheckinscheckoitDto };
    return this.ciosRepository.save(updateEmployee);
  }

  async remove(id: number) {
    const checkincheckout = await this.ciosRepository.findOneBy({ id: id });
    if (!checkincheckout) {
      throw new NotFoundException();
    }
    return this.ciosRepository.softRemove(checkincheckout);
  }
}
