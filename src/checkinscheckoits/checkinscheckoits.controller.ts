import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CheckinscheckoitsService } from './checkinscheckoits.service';
import { CreateCheckinscheckoitDto } from './dto/create-checkinscheckoit.dto';
import { UpdateCheckinscheckoitDto } from './dto/update-checkinscheckoit.dto';

@Controller('checkinscheckoits')
export class CheckinscheckoitsController {
  constructor(
    private readonly checkinscheckoitsService: CheckinscheckoitsService,
  ) {}

  @Post()
  create(@Body() createCheckinscheckoitDto: CreateCheckinscheckoitDto) {
    return this.checkinscheckoitsService.create(createCheckinscheckoitDto);
  }

  @Get()
  findAll() {
    return this.checkinscheckoitsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkinscheckoitsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCheckinscheckoitDto: UpdateCheckinscheckoitDto,
  ) {
    return this.checkinscheckoitsService.update(+id, updateCheckinscheckoitDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkinscheckoitsService.remove(+id);
  }
}
