import { Test, TestingModule } from '@nestjs/testing';
import { CheckinscheckoitsService } from './checkinscheckoits.service';

describe('CheckinscheckoitsService', () => {
  let service: CheckinscheckoitsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CheckinscheckoitsService],
    }).compile();

    service = module.get<CheckinscheckoitsService>(CheckinscheckoitsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
