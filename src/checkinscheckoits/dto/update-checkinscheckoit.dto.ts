import { PartialType } from '@nestjs/mapped-types';
import { CreateCheckinscheckoitDto } from './create-checkinscheckoit.dto';

export class UpdateCheckinscheckoitDto extends PartialType(
  CreateCheckinscheckoitDto,
) {}
