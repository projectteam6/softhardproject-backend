import { Module } from '@nestjs/common';
import { CheckinscheckoitsService } from './checkinscheckoits.service';
import { CheckinscheckoitsController } from './checkinscheckoits.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Checkinscheckoit } from './entities/checkinscheckoit.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Checkinscheckoit])],
  controllers: [CheckinscheckoitsController],
  providers: [CheckinscheckoitsService],
})
export class CheckinscheckoitsModule {}
