import { IsNotEmpty, Length } from 'class-validator';
import { Employee } from 'src/employees/entities/employee.entity';

export class CreateSalaryDto {
  // @IsNotEmpty()
  salary: number;
  // @IsNotEmpty()
  date: Date;
  employee: Employee;
}
