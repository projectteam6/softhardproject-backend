import { Employee } from 'src/employees/entities/employee.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    unique: true,
    length: '64',
  })
  login: string;

  @Column({
    length: '32',
  })
  name: string;

  @Column({
    length: '128',
  })
  password: string;

  @Column({
    length: '128',
    default: 'no-user-image.jpg',
  })
  img: string;

  @Column()
  position: string;

  @OneToOne(() => Employee)
  @JoinColumn()
  employee: Employee;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deleteddAt: Date;
}
