import { Injectable } from '@nestjs/common';
import { CreateChecklistDto } from './dto/create-checklist.dto';
import { UpdateChecklistDto } from './dto/update-checklist.dto';
import { Checklist } from './entities/checklist.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ChecklistItem } from './entities/checklist_item.entity';
import { Employee } from 'src/employees/entities/employee.entity';

@Injectable()
export class ChecklistService {
  constructor(
    @InjectRepository(Checklist)
    private checklistsRepository: Repository<Checklist>,
    @InjectRepository(ChecklistItem)
    private checlistItemRepository: Repository<ChecklistItem>,
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
  ) {}

  // create(createChecklistDto: CreateChecklistDto) {
  //   return 'This action adds a new checklist';
  // }

  async create(createChecklistDto: CreateChecklistDto) {
    // console.log(createOrderDto);

    const employee = await this.employeesRepository.findOneBy({
      id: createChecklistDto.employeeId,
    });

    const checklist: Checklist = new Checklist();
    checklist.employee = employee;
    // checklist.date = new Date();
    const oldchecklist = await this.checklistsRepository.save(checklist);

    for (const od of createChecklistDto.checklistItems) {
      const checklistItem = new ChecklistItem();
      checklistItem.name = od.name;
      checklistItem.checklist = checklist;
      checklistItem.checklist.id = oldchecklist.id;
      checklistItem.amount = od.amount;
      checklistItem.minimum = od.minimum;
      checklistItem.unit = od.unit;
      checklistItem.material = od.material;

      await this.checlistItemRepository.save(checklistItem);
      // console.log(createOrderDto);
    }
    await this.checklistsRepository.save([{ ...oldchecklist, ...checklist }]);
    return await this.checklistsRepository.findOne({
      where: { id: checklist.id },
      relations: ['checklistItems'],
    });
  }

  findAll() {
    return `This action returns all checklist`;
  }

  findAllChecklistItems() {
    return `This action returns all checklist`;
  }

  findOne(id: number) {
    return `This action returns a #${id} checklist`;
  }

  update(id: number, updateChecklistDto: UpdateChecklistDto) {
    return `This action updates a #${id} checklist`;
  }

  remove(id: number) {
    return `This action removes a #${id} checklist`;
  }
}
