import { IsNotEmpty, Length } from 'class-validator';
import { Material } from 'src/materials/entities/material.entity';

export class CreateChecklistDto {
  // @IsNotEmpty()
  // date: Date;

  @IsNotEmpty()
  employeeId: number;
  checklistItems: CreateChecklistItemDto[];
}

export class CreateChecklistItemDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  amount: number;

  @IsNotEmpty()
  minimum: number;

  @IsNotEmpty()
  unit: string;

  // @IsNotEmpty()
  material: Material;
}
