import { Employee } from 'src/employees/entities/employee.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ChecklistItem } from './checklist_item.entity';

@Entity()
export class Checklist {
  @PrimaryGeneratedColumn()
  id: number;

  // @Column()
  // date: Date;

  @ManyToOne(() => Employee, (employee) => employee.checklist)
  employee: Employee;

  @OneToMany(() => ChecklistItem, (checklistItems) => checklistItems.checklist)
  checklistItems: ChecklistItem[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deleteddAt: Date;
}
