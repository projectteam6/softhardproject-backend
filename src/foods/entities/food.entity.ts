import { OrderItem } from 'src/orders/entities/order-item';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Food {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 64, unique: true })
  name: string;

  @Column()
  type: string;

  @Column({ default: true })
  status: boolean;

  @Column({ length: '128', default: 'No_Image_Available.jpg' })
  img: string;

  @Column({ type: 'float' })
  price: number;

  @Column()
  info: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deleteddAt: Date;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.food)
  orderItems: OrderItem[];
}
