import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateFoodDto } from './dto/create-food.dto';
import { UpdateFoodDto } from './dto/update-food.dto';
import { Food } from './entities/food.entity';

@Injectable()
export class FoodsService {
  constructor(
    @InjectRepository(Food)
    private foodsRepository: Repository<Food>,
  ) {}
  create(createFoodDto: CreateFoodDto) {
    return this.foodsRepository.save(createFoodDto);
  }

  findAll() {
    return this.foodsRepository.find({ relations: ['orderItems'] });
  }

  findOne(id: number) {
    return this.foodsRepository.findOne({ where: { id } });
  }

  findType(type: string) {
    // console.log(type);
    return this.foodsRepository.find({ where: { type: type } });
  }

  findFoodBestSale() {
    const today = new Date();
    today.setHours(0, 0, 0, 0); // set time to start of the day

    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1); // set time to start of the next day

    return this.foodsRepository
      .createQueryBuilder('food')
      .leftJoin('food.orderItems', 'orderItem')
      .leftJoin('orderItem.order', 'order')
      .where(
        'order.timestampOpen >= :startOfDay AND order.timestampOpen < :startOfTomorrow',
        { startOfDay: today, startOfTomorrow: tomorrow },
      )
      .select('food.id', 'id')
      .addSelect('food.name', 'name')
      .addSelect('SUM(orderItem.qty)', 'totalQuantity')
      .groupBy('food.id')
      .orderBy('totalQuantity', 'DESC')
      .getRawMany();
  }

  findFoodBadSale(type: string) {
    const today = new Date();
    today.setHours(0, 0, 0, 0); // set time to start of the day

    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1); // set time to start of the next day
    console.log(type === 'ทั้งหมด');
    if (type === 'ทั้งหมด') {
      return this.foodsRepository
        .createQueryBuilder('food')
        .leftJoin('food.orderItems', 'orderItem')
        .leftJoin('orderItem.order', 'order')
        .where(
          'order.timestampOpen >= :startOfDay AND order.timestampOpen < :startOfTomorrow',
          { startOfDay: today, startOfTomorrow: tomorrow },
        )
        .select('food.id', 'id')
        .addSelect('food.name', 'name')
        .addSelect('SUM(orderItem.qty)', 'totalQuantity')
        .groupBy('food.id')
        .orderBy('totalQuantity', 'ASC')
        .getRawMany();
    }

    return this.foodsRepository
      .createQueryBuilder('food')
      .leftJoin('food.orderItems', 'orderItem')
      .leftJoin('orderItem.order', 'order')
      .where(
        'order.timestampOpen >= :startOfDay AND order.timestampOpen < :startOfTomorrow AND food.type = :type',
        { startOfDay: today, startOfTomorrow: tomorrow, type: type },
      )
      .select('food.id', 'id')
      .addSelect('food.name', 'name')
      .addSelect('SUM(orderItem.qty)', 'totalQuantity')
      .groupBy('food.id')
      .orderBy('totalQuantity', 'ASC')
      .getRawMany();
  }

  async update(id: number, updateFoodDto: UpdateFoodDto) {
    const food = await this.foodsRepository.findOneBy({ id });
    if (!food) {
      throw new NotFoundException();
    }
    const updatedFood = { ...food, ...updateFoodDto };

    return this.foodsRepository.save(updatedFood);
  }

  async remove(id: number) {
    const food = await this.foodsRepository.findOneBy({ id });
    if (!food) {
      throw new NotFoundException();
    }
    return this.foodsRepository.softRemove(food);
  }
}
