import { IsNotEmpty, Length } from 'class-validator';

export class CreateFoodDto {
  @IsNotEmpty()
  @Length(1, 32)
  name: string;

  @IsNotEmpty()
  type: string;

  img = 'No_Image_Available.jpg';

  @IsNotEmpty()
  price: number;

  @Length(0, 40)
  info: string;
}
