import { Module } from '@nestjs/common';
import { FoodsService } from './foods.service';
import { FoodsController } from './foods.controller';
import { TypeOrmModule } from '@nestjs/typeorm/dist';
import { Food } from './entities/food.entity';
import { Order } from 'src/orders/entities/order.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Food, Order])],
  controllers: [FoodsController],
  providers: [FoodsService],
})
export class FoodsModule {}
