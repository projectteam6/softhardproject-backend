import { Module } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { Order } from './entities/order.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderItem } from './entities/order-item';
import { Employee } from 'src/employees/entities/employee.entity';
import { Table } from 'src/tables/entities/table.entity';
import { FoodQueue } from 'src/food-queue/entities/food-queue.entity';
import { Food } from 'src/foods/entities/food.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Order,
      FoodQueue,
      OrderItem,
      Employee,
      Table,
      Food,
    ]),
  ],
  controllers: [OrdersController],
  providers: [OrdersService],
})
export class OrdersModule {}
