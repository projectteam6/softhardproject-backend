import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { OrdersService } from './orders.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';

@Controller('orders')
export class OrdersController {
  constructor(private readonly ordersService: OrdersService) {}

  @Post()
  create(@Body() createOrderDto: CreateOrderDto) {
    return this.ordersService.create(createOrderDto);
  }

  //orderItem ยังไม่ได้รับการยืนยัน
  @Get('/getStatusOrderNotPaid')
  getOrdersNotPaid() {
    return this.ordersService.getNPStatusOrder();
  }

  @Get('/getStatusOrderHavePaid')
  getOrdersHavePaid() {
    return this.ordersService.getOrdersHavePaid();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.ordersService.findOne(id);
  }

  @Get('/findTableByQr/:id')
  findTableByQr(@Param('id') id: string) {
    return this.ordersService.findTableByQr(id);
  }

  @Get()
  findAll() {
    return this.ordersService.findAll();
  }

  @Get('/findOrderWithFoodQueue')
  findOrderWithFoodQueue() {
    return this.ordersService.findOrderWithFoodQueue();
  }

  @Get('/getStatusOrder')
  getStatusOrder(@Body('status') status: string) {
    return this.ordersService.getStatusOrder(status);
  }

  // @Get('/getAllOrderItem')
  // getAllOrderItem(@Body('status') status: string) {
  //   return this.ordersService.getOrderItemAction(status);
  // }
  @Get('/getAllOrderItem')
  getAllOrderItemchef() {
    return this.ordersService.getOrderItemAction('รอทำ');
  }

  //orderItem ได้รับการยืนยัน
  @Get('/findOneOrder/:id')
  findOneOrder(@Param('id') id: string) {
    return this.ordersService.findOneOrder(id);
  }
  @Get('/findOneOrderById/:id')
  findOneOrderById(@Param('id') id: string) {
    return this.ordersService.findOneOrderById(id);
  }
  @Get(':id/getOrderItem')
  getOrderItem(@Param('id') id: string) {
    return this.ordersService.getOrderItem(id);
  }

  @Patch('/paidOrderByCash/:id/:amount/:total')
  paidOrderByCash(
    @Param('id') id: string,
    @Param('amount') amount: number,
    @Param('total') total: number,
  ) {
    return this.ordersService.paidOrderByCash(id, amount, total);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateOrderDto: UpdateOrderDto) {
    return this.ordersService.updateOrder(id, updateOrderDto);
  }

  @Patch('/changeTable/:id/:tableId')
  changeTable(@Param('id') id: string, @Param('tableId') tableId: number) {
    return this.ordersService.changTable(id, tableId);
  }

  @Patch('/addFood/:id')
  addFood(@Param('id') id: string, @Body() updateOrderDto: UpdateOrderDto) {
    return this.ordersService.addFood(id, updateOrderDto);
  }
  @Patch('/updateConfirmOrderItem/:id')
  updateConfirmOrderItem(@Param('id') id: number) {
    return this.ordersService.updateConfirmOrderItem(id);
  }

  @Patch(':id/updateStatus')
  updateOrderItemStatus(
    @Param('id') id: string,
    @Body('status') status: string,
  ) {
    return this.ordersService.updateOrderItemStatus(+id, status);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.ordersService.remove(id);
  }
  @Delete('/orderItem/:id')
  async deleteOrderItem(@Param('id') id: number) {
    return this.ordersService.deleteOrderItem(id);
  }
}
