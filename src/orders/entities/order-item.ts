import { FoodQueue } from 'src/food-queue/entities/food-queue.entity';
import { Food } from 'src/foods/entities/food.entity';
import { Table } from 'src/tables/entities/table.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Order } from './order.entity';

@Entity()
export class OrderItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ type: 'float' })
  price: number;

  @Column({ type: 'float' })
  total: number;

  @Column()
  qty: number;

  @Column()
  note: string;

  @ManyToOne(() => Food, (food) => food.orderItems)
  food: Food;

  @ManyToOne(() => Order, (order) => order.orderItems)
  order: Order;

  @OneToMany(() => FoodQueue, (foodQueue) => foodQueue.orderItem, {
    cascade: true,
  })
  foodQueue: FoodQueue[];

  @Column()
  status: string;

  @ManyToOne(() => Table, (table) => table.orderItems)
  table: Table;

  @Column({ nullable: true })
  tableId: number;

  // ...
}
