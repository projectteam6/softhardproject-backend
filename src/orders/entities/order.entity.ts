import { Employee } from 'src/employees/entities/employee.entity';
import { Table } from 'src/tables/entities/table.entity';
import { v4 as uuidv4 } from 'uuid';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { OrderItem } from './order-item';

@Entity()
export class Order {
  @PrimaryGeneratedColumn('uuid')
  id: string = uuidv4();

  @Column()
  amount: number;

  @Column({ type: 'float' })
  total: number;

  @Column()
  status: string;

  @ManyToOne(() => Table, (table) => table.orders)
  table: Table;

  @ManyToOne(() => Employee, (employee) => employee.orders)
  employee: Employee;

  @CreateDateColumn()
  timestampOpen: Date;

  @UpdateDateColumn()
  timestampClose: Date;

  @DeleteDateColumn()
  deleteddAt: Date;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItems: OrderItem[];
}
