import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import e from 'express';
import { Employee } from 'src/employees/entities/employee.entity';
import { Food } from 'src/foods/entities/food.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';
import { Table } from 'src/tables/entities/table.entity';
import { FoodQueue } from 'src/food-queue/entities/food-queue.entity';
let updateTable: Table;
@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    @InjectRepository(FoodQueue)
    private foodQueueRepository: Repository<FoodQueue>,
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
    @InjectRepository(Food)
    private foodsRepository: Repository<Food>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    @InjectRepository(Table)
    private tablesRepository: Repository<Table>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    // console.log(createOrderDto);

    const employee = await this.employeesRepository.findOneBy({
      id: createOrderDto.employeeId,
    });

    const order: Order = new Order(); //1. สร้างออเดอร์ก่อน เพื่อเอา ID ไป order Item ต้องการ ref
    order.employee = employee;
    order.table = createOrderDto.table;
    updateTable = createOrderDto.table;
    order.amount = 0;
    order.total = 0;
    order.status = createOrderDto.status;
    const oldOrder = await this.ordersRepository.save(order); // สร้างเพื่อให้ได้ ID

    for (const od of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.qty = od.qty;
      orderItem.order = order;
      orderItem.order.id = oldOrder.id;
      orderItem.food = od.food;
      orderItem.note = od.note;
      orderItem.price = od.price;
      orderItem.name = od.name;
      // orderItem.status = od.status;
      orderItem.total = od.qty * od.price;
      orderItem.table = od.table;

      await this.orderItemsRepository.save(orderItem);
      console.log(createOrderDto);

      order.amount = order.amount + orderItem.qty;
      order.total = order.total + orderItem.total;
    }
    await this.ordersRepository.save([{ ...oldOrder, ...order }]);
    return await this.ordersRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    });
  }

  findAll() {
    return this.ordersRepository.find({
      relations: ['employee', 'table', 'orderItems'],
    });
  }
  findOne(id: string) {
    return this.ordersRepository.findOne({
      where: {
        id: id,
        orderItems: {
          status: 'รอยืนยัน',
        },
      },
      relations: [
        'table',
        'employee',
        'orderItems',
        'orderItems.food',
        'orderItems.foodQueue',
      ],
    });
  }
  findTableByQr(id: string) {
    return this.ordersRepository.findOne({
      where: {
        id: id,
      },
      relations: [
        'table',
        'employee',
        'orderItems',
        'orderItems.food',
        'orderItems.foodQueue',
      ],
    });
  }
  findOneOrder(id: string) {
    return this.ordersRepository.findOne({
      where: {
        id: id,
        orderItems: {
          status: 'ยืนยันแล้ว',
        },
      },
      relations: [
        'employee',
        'orderItems',
        'orderItems.food',
        'orderItems.foodQueue',
      ],
    });
  }
  findOneOrderById(id: string) {
    return this.ordersRepository.findOne({
      where: {
        id: id,
      },
      relations: [
        'employee',
        'orderItems',
        'orderItems.food',
        'orderItems.foodQueue',
        'table',
      ],
    });
  }

  // async update(id: number, updateOrderDto: UpdateOrderDto) {
  //   const order = await this.ordersRepository.findOneBy({ id });
  //   console.log(order);
  //   if (!order) {
  //     throw new NotFoundException();
  //   }
  //   for (const od of updateOrderDto.orderItems) {
  //     const orderItem = new OrderItem();
  //     orderItem.order = order;
  //     orderItem.qty = od.qty;
  //     orderItem.order.id = id;
  //     orderItem.food = od.food;
  //     orderItem.note = od.note;
  //     orderItem.price = od.price;
  //     orderItem.name = od.name;
  //     orderItem.status = od.status;
  //     orderItem.total = od.qty * od.price;

  //     await this.orderItemsRepository.save(orderItem);
  //     order.amount += orderItem.qty;
  //     order.total += orderItem.qty * orderItem.price;
  //   }
  //   const updatedOrder = { ...order, ...updateOrderDto };
  //   console.log(updatedOrder);
  //   return this.ordersRepository.save(updatedOrder);
  // }

  async paidOrderByCash(id: string, amount: number, total: number) {
    // ดึง order ออกมาจากฐานข้อมูล
    const order: Order = await this.ordersRepository.findOne({
      where: { id: id },
    });
    order.status = 'ชำระแล้ว';
    order.amount = amount;
    order.total = total;
    return this.ordersRepository.save(order);
  }

  async addFood(id: string, updateOrderDto: UpdateOrderDto) {
    // ดึง order ออกมาจากฐานข้อมูล
    const order: Order = await this.ordersRepository.findOneBy({ id });

    // สร้างตัวแปรเพื่อเก็บ order item เดิม
    let oldOrderItem: OrderItem;

    if (!order) {
      throw new NotFoundException();
    }

    // สร้างตัวแปร table เพื่อใช้ในการอัปเดต order
    const table: Table = new Table();

    // วนลูปเพื่อสร้าง order item และ food queue ใหม่จาก updateOrderDto
    for (const od of updateOrderDto.orderItems) {
      // สร้าง order item ใหม่
      const orderItem = new OrderItem();
      orderItem.order = order;
      orderItem.qty = od.qty;
      orderItem.order.id = id;
      orderItem.food = od.food;
      orderItem.note = od.note;
      orderItem.price = od.price / od.qty;
      orderItem.name = od.name;
      orderItem.status = od.status;
      orderItem.total = od.price;
      // const table = await this.tablesRepository.findOneBy({ id: 1 });
      orderItem.table = od.table;
      console.log(od.table);

      // // สร้าง food queue ใหม่
      // for (let index = 0; index < od.qty; index++) {
      //   const foodQueue: FoodQueue = new FoodQueue();
      //   foodQueue.name = od.name;
      //   foodQueue.note = od.note;
      //   foodQueue.orderItem = orderItem;
      //   foodQueue.status = 'รอยืนยัน';

      //   // บันทึก order item เดิมและ food queue ใหม่ลงในฐานข้อมูล
      oldOrderItem = await this.orderItemsRepository.save(orderItem);
      // foodQueue.orderItem = oldOrderItem;
      // await this.foodQueueRepository.save(foodQueue);
      // }

      // อัปเดต order item ใหม่โดยอ้างอิงถึง order item เดิม
      await this.orderItemsRepository.update(oldOrderItem, orderItem);
      order.amount += orderItem.qty;
      order.total += orderItem.total;
      order.status = updateOrderDto.status;
    }

    // บันทึก order
    return this.ordersRepository.save(order);
  }

  // async updateOrder(id: string, updateOrderDto: UpdateOrderDto) {
  //   // ดึง order ออกมาจากฐานข้อมูล
  //   const order: Order = await this.ordersRepository.findOneBy({ id });
  //   order.amount = 0;
  //   order.total = 0;
  //   // สร้างตัวแปรเพื่อเก็บ order item เดิม
  //   let oldOrderItem: OrderItem;

  //   if (!order) {
  //     throw new NotFoundException();
  //   }

  //   // สร้างตัวแปร table เพื่อใช้ในการอัปเดต order
  //   const table: Table = new Table();

  //   // วนลูปเพื่อสร้าง order item และ food queue ใหม่จาก updateOrderDto
  //   for (const od of updateOrderDto.orderItems) {
  //     // สร้าง order item ใหม่
  //     const orderItem = await this.orderItemsRepository.findOneBy({
  //       id: od.id,
  //     });
  //     orderItem.order = order;
  //     orderItem.qty = od.qty;
  //     orderItem.order.id = id;
  //     orderItem.food = od.food;
  //     orderItem.note = od.note;
  //     orderItem.price = od.price / od.qty;
  //     orderItem.name = od.name;
  //     // orderItem.status = od.status;
  //     orderItem.total = od.price;
  //     // const table = await this.tablesRepository.findOneBy({ id: 1 });
  //     orderItem.table = updateTable;
  //     console.log(table);

  //     // // สร้าง food queue ใหม่
  //     // for (let index = 0; index < od.qty; index++) {
  //     //   const foodQueue: FoodQueue = new FoodQueue();
  //     //   foodQueue.name = od.name;
  //     //   foodQueue.note = od.note;
  //     //   foodQueue.orderItem = orderItem;
  //     //   foodQueue.status = 'รอทำ';

  //     //   // บันทึก order item เดิมและ food queue ใหม่ลงในฐานข้อมูล
  //     // oldOrderItem = await this.orderItemsRepository.save(orderItem);
  //     //   foodQueue.orderItem = oldOrderItem;
  //     //   await this.foodQueueRepository.save(foodQueue);
  //     // }

  //     // อัปเดต order item ใหม่โดยอ้างอิงถึง order item เดิม
  //     await this.orderItemsRepository.update(orderItem.id, orderItem);
  //     order.amount = orderItem.qty;
  //     order.total = orderItem.total;
  //     order.status = updateOrderDto.status;
  //   }

  //   // บันทึก order
  //   return this.ordersRepository.save(order);
  // }

  async updateOrder(id: string, updateOrderDto: UpdateOrderDto) {
    // ดึง order ออกมาจากฐานข้อมูล
    const order: Order = await this.ordersRepository.findOne({
      where: { id: updateOrderDto.id },
      relations: ['orderItem'],
    });
    // order.amount = 0;
    // order.total = 0;
    // สร้างตัวแปรเพื่อเก็บ order item เดิม
    let oldOrderItem: OrderItem;

    if (!order) {
      throw new NotFoundException();
    }

    // สร้างตัวแปร table เพื่อใช้ในการอัปเดต order
    const table: Table = new Table();

    // วนลูปเพื่อสร้าง order item และ food queue ใหม่จาก updateOrderDto
    for (const od of updateOrderDto.orderItems) {
      // สร้าง order item ใหม่
      const orderItem = await this.orderItemsRepository.findOneBy({
        id: od.id,
      });
      orderItem.id = od.id;
      orderItem.order = order;
      orderItem.qty = od.qty;
      orderItem.order.id = id;
      orderItem.food = od.food;
      orderItem.note = od.note;
      orderItem.price = od.price / od.qty;
      orderItem.name = od.name;
      // orderItem.status = od.status;
      orderItem.total = od.price;
      // const table = await this.tablesRepository.findOneBy({ id: 1 });
      orderItem.table = od.table;
      console.log(table);

      // // สร้าง food queue ใหม่
      // for (let index = 0; index < od.qty; index++) {
      //   const foodQueue: FoodQueue = new FoodQueue();
      //   foodQueue.name = od.name;
      //   foodQueue.note = od.note;
      //   foodQueue.orderItem = orderItem;
      //   foodQueue.status = 'รอทำ';

      //   // บันทึก order item เดิมและ food queue ใหม่ลงในฐานข้อมูล
      // oldOrderItem = await this.orderItemsRepository.save(orderItem);
      //   foodQueue.orderItem = oldOrderItem;
      //   await this.foodQueueRepository.save(foodQueue);
      // }

      // อัปเดต order item ใหม่โดยอ้างอิงถึง order item เดิม
      const newOrderItem = await this.orderItemsRepository.save(orderItem);
      order.amount += orderItem.qty;
      order.total += orderItem.total;
      order.status = updateOrderDto.status;
    }

    // บันทึก order
    console.log(updateOrderDto);
    return this.ordersRepository.save(order);
  }

  async remove(id: string) {
    const order = await this.ordersRepository.findOneBy({ id: id });
    if (!order) {
      throw new NotFoundException();
    }
    return this.ordersRepository.softRemove(order);
  }

  async changTable(id: string, idTable: number) {
    const order = await this.ordersRepository.findOne({
      where: { id: id },
      relations: ['orderItems'],
    });
    if (!order) {
      throw new NotFoundException();
    }
    const table: Table = await this.tablesRepository.findOne({
      where: { id: idTable },
    });
    if (!table) {
      throw new NotFoundException();
    }
    // for (const od of table) {

    order.table = table;
    // orderItem.status = status;
    await this.ordersRepository.save(order);
    order.orderItems.forEach(async (odit) => {
      odit.table = table;
      await this.orderItemsRepository.save(odit);
    });
    return order;
  }

  async updateOrderItemStatus(orderItemId: number, status: string) {
    const orderItem = await this.orderItemsRepository.findOneBy({
      id: orderItemId,
    });
    if (!orderItem) {
      throw new NotFoundException();
    }
    // orderItem.status = status;
    await this.orderItemsRepository.save(orderItem);
    return orderItem;
  }

  async updateConfirmOrderItem(orderItemId: number) {
    const orderItem = await this.orderItemsRepository.findOneBy({
      id: orderItemId,
    });
    if (!orderItem) {
      throw new NotFoundException();
    }
    orderItem.status = 'ยืนยันแล้ว';
    await this.orderItemsRepository.save(orderItem);
    return orderItem;
  }

  async getOrderItem(id: string) {
    const order = await this.ordersRepository.findOne({
      where: { id: id },
      relations: ['employee', 'orderItems'],
    });
    return order.orderItems;
  }
  async getStatusOrder(status: string) {
    const order = await this.ordersRepository.find({
      where: { status: status },
      // relations: ['employee', 'orderItems'],
    });
    return order;
  }

  async getNPStatusOrder() {
    const order = await this.ordersRepository.find({
      where: { status: 'ยังไม่ชำระ' },
      relations: ['table', 'orderItems', 'orderItems.foodQueue'],
    });
    return order;
  }

  async getOrdersHavePaid() {
    const today = new Date();
    today.setHours(0, 0, 0, 0); // set time to start of the day

    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1); // set time to start of the next day
    // const order = await this.ordersRepository.find({
    //   where: { status: status },
    //   relations: ['table', 'orderItems'],
    // });
    return this.ordersRepository
      .createQueryBuilder('order')
      .leftJoinAndSelect('order.orderItems', 'orderItem')
      .leftJoinAndSelect('order.table', 'table')
      .where(
        'order.status = :statusOrder AND order.timestampOpen >= :startOfDay AND order.timestampOpen < :startOfTomorrow',
        {
          statusOrder: 'ชำระแล้ว',
          startOfDay: today,
          startOfTomorrow: tomorrow,
        },
      )
      .andWhere('order.status = :statusOrder', { statusOrder: 'ชำระแล้ว' })
      .getMany();
  }

  findAllOrderItems(status: string) {
    return this.orderItemsRepository
      .createQueryBuilder('orderItem')
      .leftJoinAndSelect('orderItem.order', 'order')
      .leftJoinAndSelect('order.employee', 'employee')
      .leftJoinAndSelect('order.table', 'table')
      .where('orderItem.status = :status', { status: status })
      .getMany();
  }

  async getOrderItemAction(txt: string) {
    return this.findAllOrderItems(txt);
  }
  findOrderWithFoodQueue() {
    return this.foodQueueRepository
      .createQueryBuilder('foodQueue')
      .leftJoinAndSelect('foodQueue.orderItem', 'orderItem')
      .leftJoinAndSelect('orderItem.order', 'order')
      .getMany();
  }
  async deleteOrderItem(id: number) {
    const orderItem = await this.orderItemsRepository.findOne({
      where: { id: id },
      relations: ['order'],
    });
    const order = await this.ordersRepository.findOneBy({
      id: orderItem.order.id,
    });
    console.log('delete orderItem ' + JSON.stringify(orderItem));
    if (!orderItem) {
      throw new NotFoundException('Order Item not found');
    }
    // Delete the related FoodQueue first
    await this.foodQueueRepository.delete({ orderItem: orderItem });
    // Then delete the OrderItem
    order.amount -= orderItem.qty;
    order.total -= orderItem.price * orderItem.qty;
    await this.ordersRepository.update(order.id, order);
    const res = await this.orderItemsRepository.remove(orderItem);
    return res;
  }
}
