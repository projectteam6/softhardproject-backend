import { IsNotEmpty, Length, Min } from 'class-validator';
import { FoodQueue } from 'src/food-queue/entities/food-queue.entity';
import { Food } from 'src/foods/entities/food.entity';
import { Table } from 'src/tables/entities/table.entity';

class CreateOrderItemDto {
  id: number;

  // @IsNotEmpty()
  foodId: number;

  // @IsNotEmpty()
  qty: number;
  food: Food;
  note: string;
  price: number;
  status: string;
  name: string;
  table: Table;
  foodQueue: FoodQueue[];
}

export class CreateOrderDto {
  // @IsNotEmpty()
  employeeId: number;
  id: string;
  // @IsNotEmpty()
  // tableId: number;
  table: Table;
  // @IsNotEmpty()
  status: string;

  orderItems: CreateOrderItemDto[];
}
