import { IsNotEmpty, Length, Min } from 'class-validator';

export class CreateTableDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  // @Min(0)
  amount: number;

  @IsNotEmpty()
  status: string;
}
