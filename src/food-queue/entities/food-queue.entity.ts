import { Employee } from 'src/employees/entities/employee.entity';
import { OrderItem } from 'src/orders/entities/order-item';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class FoodQueue {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 64 })
  name: string;

  @Column()
  status: string;

  @Column()
  note: string;

  @Column()
  chefName: string;

  @Column()
  waiterName: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deleteddAt: Date;

  @ManyToOne(() => Employee, (employee) => employee.foodQueue)
  employee: Employee;

  @ManyToOne(() => OrderItem, (orderItem) => orderItem.foodQueue)
  orderItem: OrderItem;
}
