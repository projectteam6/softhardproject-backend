import { PartialType } from '@nestjs/mapped-types';
import { CreateFoodQueueDto } from './create-food-queue.dto';

export class UpdateFoodQueueDto extends PartialType(CreateFoodQueueDto) {}
