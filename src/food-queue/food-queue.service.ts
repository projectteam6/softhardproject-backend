import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OrderItem } from 'src/orders/entities/order-item';
import { Repository } from 'typeorm';
import { CreateFoodQueueDto } from './dto/create-food-queue.dto';
import { UpdateFoodQueueDto } from './dto/update-food-queue.dto';
import { FoodQueue } from './entities/food-queue.entity';

@Injectable()
export class FoodQueueService {
  constructor(
    @InjectRepository(FoodQueue)
    private foodQueueRepository: Repository<FoodQueue>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
  ) {}

  async create(createFoodQueueDto: CreateFoodQueueDto) {
    console.log(createFoodQueueDto);
    const orderItem = await this.orderItemsRepository.findOneBy({
      id: createFoodQueueDto.orderItem.id,
    });

    // const orderItem: OrderItem = createFoodQueueDto.orderItem;
    const foodQueue: FoodQueue = new FoodQueue();
    console.log('Is orderItem: ' + createFoodQueueDto.orderItem.id);

    foodQueue.chefName = '';
    foodQueue.name = createFoodQueueDto.name;
    foodQueue.note = createFoodQueueDto.note;
    foodQueue.status = createFoodQueueDto.status;
    foodQueue.waiterName = '';
    // foodQueue.orderItem = orderItem;

    return this.foodQueueRepository.save(createFoodQueueDto);
  }

  // findAll() {
  //   return this.foodQueueRepository.find({
  //     relations: ['orderItem'],
  //   });
  // }
  findAll() {
    return this.foodQueueRepository
      .createQueryBuilder('food-queue')
      .leftJoinAndSelect('food-queue.orderItem', 'orderItem')
      .leftJoinAndSelect('orderItem.table', 'table')
      .leftJoinAndSelect('orderItem.food', 'food')
      .leftJoinAndSelect('orderItem.order', 'order')
      .getMany();
  }

  //เอา food-queue ทั้งหมด สถานะ = รอทำ
  findAllChef() {
    return this.foodQueueRepository
      .createQueryBuilder('food-queue')
      .leftJoinAndSelect('food-queue.orderItem', 'orderItem')
      .leftJoinAndSelect('orderItem.table', 'table')
      .where('food-queue.status = :status', { status: 'รอทำ' })
      .getMany();
  }

  //เอา food-queue ทั้งหมด ระบุสถานะ FoodQueue = ?
  findFoodQueueByStatus(status: string, idOrder: string) {
    return this.foodQueueRepository
      .createQueryBuilder('food-queue')
      .leftJoinAndSelect('food-queue.orderItem', 'orderItem')
      .leftJoinAndSelect('orderItem.order', 'order')
      .leftJoinAndSelect('order.table', 'table')
      .where('order.status = :statusOrder', { statusOrder: 'ยังไม่ชำระ' })
      .where('order.id = :idOrder', { idOrder })
      .andWhere('food-queue.status = :status', { status })
      .getMany();
  }

  //เอา food-queue ทั้งหมด ไม่สนสถานะ FoodQueue
  findFoodQueueNotUnPaid(idTable: number) {
    return this.foodQueueRepository
      .createQueryBuilder('food-queue')
      .leftJoinAndSelect('food-queue.orderItem', 'orderItem')
      .leftJoinAndSelect('orderItem.order', 'order')
      .leftJoinAndSelect('order.table', 'table')
      .andWhere('table.id = :idTable', { idTable })
      .andWhere('order.status = :statusOrder', { statusOrder: 'ยังไม่ชำระ' })
      .getMany();
  }
  findFoodQueueByOrderId(idOrder: string) {
    return this.foodQueueRepository
      .createQueryBuilder('food-queue')
      .leftJoinAndSelect('food-queue.orderItem', 'orderItem')
      .leftJoinAndSelect('orderItem.order', 'order')
      .leftJoinAndSelect('order.table', 'table')
      .andWhere('order.id = :orderId', { orderId: idOrder })
      .andWhere('order.status = :statusOrder', { statusOrder: 'ยังไม่ชำระ' })
      .getMany();
  }
  // เอา food-queue ทั้งหมด ตามชื่อพ่อครัว
  findMyFoodQueue(findChefName: string) {
    return this.foodQueueRepository
      .createQueryBuilder('food-queue')
      .leftJoinAndSelect('food-queue.orderItem', 'orderItem')
      .leftJoinAndSelect('orderItem.table', 'table')
      .where('food-queue.chefName = :chefName', { chefName: findChefName })
      .andWhere('food-queue.status = :status', { status: 'กำลังทำ' })
      .getMany();
  }

  // findOnePendingConfirmation(id: number) {
  //   return this.foodQueueRepository.findOne({
  //     where: { id, status: 'รอยืนยัน' },
  //   });
  // }

  findOne(id: number) {
    return this.foodQueueRepository.findOne({
      where: { id },
      relations: ['orderItem.food'],
    });
  }

  async update(id: number, updateFoodQueueDto: UpdateFoodQueueDto) {
    const foodQueue = await this.foodQueueRepository.findOneBy({ id: id });
    if (!foodQueue) {
      throw new NotFoundException();
    }
    const updateFoodQueue = { ...foodQueue, ...updateFoodQueueDto };
    return this.foodQueueRepository.save(updateFoodQueue);
  }

  async remove(id: number) {
    const foodqueue = await this.foodQueueRepository.findOneBy({ id: id });
    if (!foodqueue) {
      throw new NotFoundException();
    }
    return this.foodQueueRepository.remove(foodqueue);
  }
}
