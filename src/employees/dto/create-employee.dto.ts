import { IsNotEmpty, Length } from 'class-validator';
export class CreateEmployeeDto {
  @IsNotEmpty()
  @Length(1, 32)
  firstname: string;

  @IsNotEmpty()
  @Length(1, 32)
  lastname: string;

  @IsNotEmpty()
  age: number;

  @IsNotEmpty()
  identityNum: string;

  @IsNotEmpty()
  email: string;

  @IsNotEmpty()
  birthday: Date;

  @IsNotEmpty()
  @Length(1, 64)
  role: string;

  @IsNotEmpty()
  hourrate: number;
}
