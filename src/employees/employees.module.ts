import { Module } from '@nestjs/common';
import { EmployeesService } from './employees.service';
import { EmployeesController } from './employees.controller';
import { Employee } from './entities/employee.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FoodQueue } from 'src/food-queue/entities/food-queue.entity';
import { Salary } from 'src/salarys/entities/salary.entity';
import { Order } from 'src/orders/entities/order.entity';
import { Checklist } from 'src/checklist/entities/checklist.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Employee, FoodQueue, Salary, Order, Checklist]),
  ],
  controllers: [EmployeesController],
  providers: [EmployeesService],
})
export class EmployeesModule {}
