import { Transform } from 'class-transformer';
import { TransformFnParams } from 'class-transformer/types/interfaces';
import { Checkinscheckoit } from 'src/checkinscheckoits/entities/checkinscheckoit.entity';
import { Checklist } from 'src/checklist/entities/checklist.entity';
import { FoodQueue } from 'src/food-queue/entities/food-queue.entity';
import { Order } from 'src/orders/entities/order.entity';
import { Salary } from 'src/salarys/entities/salary.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  JoinColumn,
} from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  firstname: string;

  @Column({
    length: '32',
  })
  lastname: string;

  @Column()
  age: number;

  @Column()
  identityNum: string;

  @Column()
  email: string;

  @Column('date', { nullable: true })
  birthday: Date;

  @Column()
  role: string;

  @Column()
  hourrate: number;

  @OneToMany(() => Order, (order) => order.employee)
  orders: Order[];

  @OneToMany(() => Salary, (salary) => salary.employee)
  salary: Salary[];

  @OneToMany(() => Checklist, (checklist) => checklist.employee)
  checklist: Checklist[];

  @OneToMany(
    () => Checkinscheckoit,
    (checkincheckout) => checkincheckout.employee,
  )
  checkincheckout: Checkinscheckoit[];

  @OneToMany(() => FoodQueue, (foodQueue) => foodQueue.employee)
  foodQueue: FoodQueue[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deleteddAt: Date;
}
function moment(date1: TransformFnParams) {
  throw new Error('Function not implemented.');
}
